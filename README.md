Summary:
Pun Generator is a python application that utilizes light NLP and webscraping to produce puns.

Process:
1. User inputs a word (word_1)
2. Pun Generator finds the pronunciation of word_1 via the Carnegie Mellon Pronunciation Dictionary corpus
3. P.G. finds another word (word_2) whose pronunciation's edit distance from word_1 is minimal
4. P.G. finds a sentence in which word_2 is used naturally
5. P.G. replaces word_2 with word_1 in said sentence
6. P.G. prints puns

In short:
P.G. makes a pun by finding phonetically similar words and switching their places in a sentence

Limitations:
P.G. only recognizes the 100,000 most common English words as input.
P.G. only runs on Python3
