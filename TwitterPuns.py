import tweepy
import time
from nltk.corpus import stopwords
from PunGenerator import PunGenerator

CONSUMER_KEY = "qaV4wnDlJX5cOnyex2Z1hFRlC"
CONSUMER_SECRET = "lcACkMMIaIIFxlUBQwI3MNmspWRv0kcPLOLFsXyq30XKb3viHJ"
ACCESS_KEY = "1253159169387102208-uQmdkM6gZP0PjM7IAhNVq1pAH1Ldme"
ACCESS_SECRET = "OH78AmUuFfVwx08LXSsj1lxAuufxqynht04CmTGUwf2UM"

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth, wait_on_rate_limit=True)

stop_words = set(stopwords.words("english"))
pun_generator = PunGenerator()
FILE_NAME = "last_seen_id.txt"


def reply_to_tweets():
	try:
		mentions = api.mentions_timeline()
		last_seen_tweet_id = retrieve_last_seen_id(FILE_NAME)


		if (mentions):
			ID = mentions[0].id
			tweet = mentions[0].text
			screen_name = mentions[0].user.screen_name

			words = tweet.split()

			words_to_punify = [word for word in words if (word != "@ThePunGenerator" and word not in stop_words)]

			if ID != last_seen_tweet_id:
				store_last_seen_id(ID, FILE_NAME)
				puns = pun_generator.generate_puns(words_to_punify[0].upper())

				if (puns):
					print("@{} here's your pun for {}\n{}".format(screen_name, words_to_punify[0], puns[0]))
					api.update_status("@{} here's your pun for {}\n{}".format(screen_name, words_to_punify[0], puns[0]))
				else:
					api.update_status("@{} I am not yet smart enough to come up with a pun for {}.\nYou're on your own.".format(screen_name, words_to_punify[0]))
	except tweepy.error.TweepError:
		print("Error retrieving mentions. Connection may be down...Retrying")


def store_last_seen_id(last_seen_id, file_name):
	with open(file_name, 'w') as f:
		f.write(str(last_seen_id))


def retrieve_last_seen_id(file_name):
	with open(file_name, 'r') as f:
		return int(f.read().strip())



while (True):
	reply_to_tweets()
	time.sleep(15)


"""
TODO:
-Randomize the choice of the pun from the list. First 10-15 are probably most common uses of the match word
"""
