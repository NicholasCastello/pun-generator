from PunGenerator import PunGenerator

import time

def display_puns(puns, start_time):
    num_puns = len(puns)
    max_puns_to_show = 5
    show_more = True
    count = 0

    # Stop duration clock
    elapsed_time = time.time() - start_time
    print("Program ran in {} seconds".format(round(elapsed_time, 3)))

    if(num_puns < max_puns_to_show):
        max_puns_to_show = num_puns

    while (show_more):
        try:
            for i in range(max_puns_to_show):
                i = i + (count * 5)
                print("\nPun {}) {}".format(i+1, puns[i]))
            choice = input("\nWould you like to see more puns for this word?(Y/N): ").lower()
            if choice == 'n':
                show_more = False
            count += 1
        except IndexError:
            print("No more puns for this word!")


# general algo
def main():
    pun_generator = PunGenerator()

    # Get the user input
    word = input("Enter a word for a pun: ").upper()

    # Start duration clock
    start_time = time.time()

    puns_generated = pun_generator.generate_puns(word)

    if(puns_generated):
        display_puns(puns_generated, start_time)
    else:
        print("No puns generated!")



if __name__=="__main__":
    main()